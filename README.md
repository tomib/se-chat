# SE Chat

A Chat Application concept with a large data set as requested in the test task. Due to the four hours time restriction, this is at best a foundation to build upon. Basic functionality is done, but more work is required feature and performance-wise. Optimization is needed and implementation of some sort of a filter and limiter on the amount of conversations displayed at once.

Demo is available on: http://www.tomislavbisof.com/se-chat/

### Installation

To install all the required dependencies, run:

```
npm install
```

### Usage

For development with the Webpack Dev Server and HMR, use:

```
npm run watch
```

To build the project for production, use:

```
npm run build
```

Your project files will be located in the `/dist/` folder after building. Images smaller than 8KB will be embedded, while larger ones will be located in `/dist/assets/`.

To run tests (actually just one test - there was no time!), use:

```
npm run test
```

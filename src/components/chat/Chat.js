import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Sidebar from './sidebar/Sidebar';
import Conversation from './conversation/Conversation';

import chatData from '../../assets/chatdata.json';

import './Chat.scss';

class Chat extends Component {
  state = {
    chatData: [],
    chatIdList: [],
    loaded: false,
  }

  componentDidMount() {
    /* Once the component is loaded, prepare the JSON data for usage. */
    this.prepareData();
  }

  prepareData = () => {
    /* Go through the JSON object and sort the conversations by most recent reply. */
    this.setState(() => ({
      chatData: chatData.sort((a, b) => {
        if (a.transcript && a.transcript.length > 0 && b.transcript && b.transcript.length > 0) {
          return b.transcript[b.transcript.length - 1].date - a.transcript[a.transcript.length - 1].date;
        }

        /* If the transcript array doesn't exist, use the created_at field for sorting. */
        return b.created_at - a.created_at;
      }),
    }), () => { this.generateChatIdList(); });
  }

  generateChatIdList = () => {
    /* Create an array of conversation IDs with the same order as the main data array. This is used to fetch object positions more easily without having to loop through the entire array each time. */
    this.setState(() => ({
      chatIdList: this.state.chatData.map(item => item.id),
      loaded: true,
    }));
  }

  fetchChatData = (chatId) => {
    /* Returns the conversation data if a route param containing the ID of a conversation exists. */
    if (chatId && this.state.chatIdList.includes(chatId)) {
      return this.state.chatData[this.state.chatIdList.indexOf(chatId)].transcript || [];
    }

    return null;
  };

  addReply = (message, timestamp) => {
    /* This method handles adding replies to the conversation. It creates a new data array with the reply data based on the current one. */
    if (message) {
      const data = [...this.state.chatData];
      const dataPosition = this.state.chatIdList.indexOf(this.props.match.params.id);

      if (!data[dataPosition].transcript) {
        data[dataPosition].transcript = [];
      }

      data[dataPosition].transcript.push({
        id: this.props.match.params.id,
        date: timestamp,
        alias: 'SnapEngage Test Agent',
        message,
      });

      this.setState(() => ({
        chatData: data,
      }));
    }
  }

  render() {
    return this.state.loaded ? (
      <div className="chat">
        <Sidebar
          data={this.state.chatData}
        />
        <Conversation
          data={this.fetchChatData(this.props.match.params.id)}
          onSendMessage={this.addReply}
        />
      </div>
    ) : (
      <div className="chat--loading">Loading the chat application...</div>
    );
  }
}

Chat.propTypes = {
  match: PropTypes.object.isRequired,
};

export default Chat;

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Scrollbars } from 'react-custom-scrollbars';

import Message from './message/Message';
import Reply from './reply/Reply';

import './Conversation.scss';

import ChatIcon from '../../../assets/chat.svg';

class Conversation extends Component {
  constructor(props) {
    super(props);
    this.scrollbarRef = React.createRef();
  }

  componentDidUpdate() {
    if (this.props.data) {
      this.scrollbarRef.current.scrollToBottom();
    }
  }

  render() {
    return this.props.data ? (
      <div className="chat__conversation">
        <div className="chat__messages">
          <Scrollbars ref={this.scrollbarRef}>
            {this.props.data.map((item, index) => <Message key={index.toString()} data={item} />)}
          </Scrollbars>
        </div>
        <Reply onSendMessage={this.props.onSendMessage} />
      </div>
    ) : (
      <div className="chat__conversation">
        <div className="chat__conversationEmpty">
          <img src={ChatIcon} alt="" />
          <span>Please select a conversation from the menu on the left.</span>
        </div>
      </div>
    );
  }
}

Conversation.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object),
  onSendMessage: PropTypes.func.isRequired,
};

Conversation.defaultProps = {
  data: null,
};

export default Conversation;

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import './Reply.scss';

class Reply extends Component {
  state = {
    message: '',
  }

  onMessageChange = (message) => {
    this.setState(() => ({
      message,
    }));
  }

  onSubmitreply = (e) => {
    e.preventDefault();

    this.props.onSendMessage(this.state.message, moment().valueOf());
    this.setState(() => ({
      message: '',
    }));
  }

  render() {
    return (
      <form className="chat__reply" onSubmit={this.onSubmitreply}>
        <div className="chat__replyLeft">
          <input
            className="chat__replyInput"
            onChange={e => this.onMessageChange(e.target.value)}
            placeholder="Write a reply here..."
            type="text"
            value={this.state.message}
          />
        </div>
        <div className="chat__replyRight">
          <button
            className="chat__replyButton"
            disabled={this.state.message.trim().length === 0}
            type="submit"
          >
            Send
          </button>
        </div>
      </form>
    );
  }
}

Reply.propTypes = {
  onSendMessage: PropTypes.func.isRequired,
};

export default Reply;

import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import './Message.scss';

const Message = props => (
  <div className={`chat__message ${props.data.id ? 'chat__message--support' : 'chat__message--customer'}`}>
    <div className="chat__messageContainer">
      <div className="chat__messageText">{props.data.message}</div>
      <div className="chat__messageDetails">
        <strong>{props.data.alias}</strong> at {moment(props.data.date).format('Do MMM YYYY, HH:mm:ss')}
      </div>
    </div>
  </div>
);

Message.propTypes = {
  data: PropTypes.object.isRequired,
};

export default Message;

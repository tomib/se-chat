import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Scrollbars } from 'react-custom-scrollbars';

import Item from './item/Item';

import './Sidebar.scss';

const Sidebar = props => (
  <div className="chat__sidebar">
    <Scrollbars>
      { props.data.map(item => (
        <Item
          key={item.id}
          data={{
            id: item.id,
            customer: item.requested_by,
            lastMessage: item.transcript ? item.transcript[item.transcript.length - 1].message : item.initial_message,
            lastActive: item.transcript ? moment(item.transcript[item.transcript.length - 1].date).format('Do MMM YYYY, HH:mm:ss') : moment(item.created_at).format('Do MMM YYYY, HH:mm:ss'),
          }}
        />))}
    </Scrollbars>
  </div>
);

Sidebar.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default Sidebar;

import React from 'react';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';

import './Item.scss';

const Item = props => (
  <NavLink
    activeClassName="chat__sidebarItem--active"
    className="chat__sidebarItem"
    to={`/chat/${props.data.id}`}
  >
    <div className="chat__sidebarItemCustomer">{props.data.customer}</div>
    <div className="chat__sidebarItemDate"> {props.data.lastActive}</div>
    <div className="chat__sidebarItemMessage">{props.data.lastMessage.length > 60 ? `${props.data.lastMessage.substring(0, 60)}...` : props.data.lastMessage}</div>
  </NavLink>
);

Item.propTypes = {
  data: PropTypes.shape({
    customer: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    lastActive: PropTypes.string.isRequired,
    lastMessage: PropTypes.string.isRequired,
  }).isRequired,
};

export default Item;

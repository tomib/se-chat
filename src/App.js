import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import Chat from './components/chat/Chat';
import './App.scss';

const App = () => (
  <div className="app">
    <Switch>
      <Route path="/chat/:id?" component={Chat} />
      <Redirect to="/chat" />
    </Switch>
  </div>
);

export default App;
